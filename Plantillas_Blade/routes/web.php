<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// i created a controller 
Route::get('user/{id}', 'UserController@show');

Route::get('/','UserController@sayHi');
/*
//i returned parameters with the router
Route::get('/', function(){
    return view('users', ['name'=> 'jose']);
});
*/
//returning the function with the controller