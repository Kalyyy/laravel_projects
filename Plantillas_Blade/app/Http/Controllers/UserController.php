<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function show($id){
        return 'user id: '.$id;
    }
    public function sayHi(){
        return view('users', ['name'=> 'Kaly']);
    }
}
